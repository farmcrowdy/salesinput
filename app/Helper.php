<?php

namespace App;

//use App\Http\Controllers\EmailController;
use App\Models\Config;
use App\Models\Status;
use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserCode;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class Helper extends Model
{
    /**
     * validates an associative array empty fields
     * @param array $fields
     * @return string
     */
    static function checkEmptyFields(array $fields){
        $result = '';
        if(count($fields) > 0){
            foreach($fields as $k => $v){
                if(empty($v) || !isset($v) || is_null($v)){
                    $result = "The $k field is required.";
                    return $result;
                }
            }
        }

        return $result;
    }

    /**
     * get mobile token
     * @return int|string
     */
    static function getMobileOTP(){
        return mt_rand(10000, 99999);
    }

    //checks if an OTP has expired
    /**
     * @param $updated_at
     * @return bool
     */
    public static function validateOTP($updated_at){
        $max_time = 5;//minutes
        $now = Carbon::now();
        $difference = $now->diffInMinutes($updated_at);
        if($difference > $max_time) return true;
        return false;
    }

    /**
     * creates a user's access token
     * @param $UserModel
     * @return string
     */
    static function createAccessToken($UserModel) {
        $accessToken = '';
        try{
            $token = $UserModel->createToken('accessToken')->accessToken;
            if($token){
                $accessToken = $token;
            }

        } catch (Exception $e){return $accessToken = '';}
        return $accessToken;
    }


    /**
     * validate a string to contain only strings
     * @param $string
     * @return string
     */

    static function validateNamedString($string){
        $result = '';
        $stripped = str_replace(' ', '', $string);//remove all white spaces and validate string
        if(preg_match("/^[a-zA-Z'-]+$/", $stripped)){//if string is a valid one
            $result = '1';
            return $result;
        }
        return $result;
    }

    /**
     * @param int $status_id
     * @return mixed
     */
    static function statusName(int $status_id)
    {
        return Status::where('id', $status_id)->get()[0]->name;
    }

    static function getStatusIdByName(string $status_name)
    {
        return Status::where('name', $status_name)->get()[0]->id;
    }

    /**
     * send an OTP to the users' email address
     * @param \App\User $user
     * @param int $otp
     * @param string $subject
     * @return bool
     * @throws \SendGrid\Mail\TypeException
     */
    static function emailOTP(User $user, int $otp, $subject = 'Reset your password'){
        $d = [
            'recipient_name' => $user->name,
            'otp' => $otp
        ];
        $configData = [
            'sender_email'=> Helper::config('site_email'),
            'sender_name'=> Helper::config('site_name'),
            'recipient_email'=> $user->email,
            'recipient_name'=> $user->name,
            'subject'=> $subject
        ];

        try {
            EmailController::dispatchMail($configData, 'emails.otp_email', $d);

        } catch (TypeException $e) {return false;}
    }

    /**
     * @param $user
     * @param int $otp
     * @param string $subject
     * @return bool
     * @throws \SendGrid\Mail\TypeException
     */
    static function newEmailOTP($user, int $otp, $subject = null)
    {
        $name = $user->name;
        $email = $user->email;
        $d = [
            'recipient_name' => $name,
            'otp' => $otp
        ];
        $configData = [
            'sender_email'=> Helper::config('site_email'),
            'sender_name'=> Helper::config('site_name'),
            'recipient_email'=> $email,
            'recipient_name'=> $name,
            'subject'=> $subject
        ];

        try {
            EmailController::dispatchMail($configData, 'emails.signup_otp_email', $d);
            return true;
        } catch (TypeException $e) {return false;}
    }

    /**
     * @param \App\User $user
     * @param $orders
     * @return bool
     * @throws \SendGrid\Mail\TypeException
     */
    static function ongoingOrderEmail(User $user, $orders, $transaction)
    {
        $subject = 'You have placed an Order';
        $d = [
            'recipient_name' => $user->name,
            'orders' => $orders,
            'user' => $user,
            'transaction' => $transaction
        ];
        $configData = [
            'sender_email'=> 'info@farmcrowdy.com',
            'sender_name'=> 'Farmcrowdy Foods',
            'recipient_email'=> $user->email,
            'recipient_name'=> $user->name,
            'subject'=> $subject
        ];

        try {
            EmailController::dispatchMail($configData, 'emails.receipt', $d);
            return true;
        } catch (TypeException $e) {return false;}

    }

    static function adminOngoingOrderEmail(User $user, $orders, $transaction)
    {
        $subject = 'Customer placed an Order';
        $d = [
            'recipient_name' => $user->name,
            'orders' => $orders,
            'user' => $user,
            'transaction' => $transaction
        ];
        $configData = [
            'sender_email'=> Helper::config('site_email'),
            'sender_name'=> Helper::config('site_name'),
            'recipient_email'=> 'orders@farmcrowdy.com',
            'recipient_name'=> $user->name,
            'subject'=> $subject
        ];

        try {
            EmailController::dispatchMail($configData, 'emails.receipt', $d);
            return true;
        } catch (TypeException $e) {return false;}

    }

    /**
     * this is the function for returning json responses
     * @param $status
     * @param $message
     * @param null $statusCode
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    static function jsonResponse(bool $status, string $message, int $statusCode = null, $data = null){
        //the code means the response code like 200, 404, 400. 419, etc
        $trueRes = ['status'=> $status, 'message'=> $message, 'data'=>$data];
        $falseRes = ['status'=> $status, 'message'=> $message];
        $finalRes = null;
        $status === true ? $finalRes = $trueRes : $finalRes = $falseRes;
        if(is_null($statusCode)){
            return response()->json($finalRes);
        }
        return response()->json($finalRes, $statusCode);
    }

    /**
     * @param string $status_name
     * @return mixed
     */
    static function statusIdByName(string $status_name)
    {
        return Status::where('name', $status_name)->get()[0]->name;
    }



    /**
     * a function to validate a user email address;
     * @param $email
     * @return string
     */
    public static function isNotValidEmail($email){
        $msg = '';
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $msg = 'EmailHelper address not valid.';
        }
        return $msg;
    }

    static function config(string $config_name) {
        $configArray = Config::where('name', $config_name)->first();
        if($configArray) {
            return $configArray->value;
        } else {
            return null;
        }
    }

    //this is the email dispatcher
    function sendEmail($sender_email, $sender_name, $recipient_email, $recipient_name, $subject, $body){
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("test@example.com", "Example User");
        $email->setSubject("Sending with SendGrid is Fun");
        $email->addTo("atomifak@gmail.com", "Tomiiwo");
        $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
        $email->addContent(
            "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
        );

        $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
        try {
            $response = $sendgrid->send($email);
            $status = $response->statusCode();
            //sent here
            //        print $response->statusCode() . "\n";
            //        print_r($response->headers());
            //        print $response->body() . "\n";
        } catch (Exception $e) {
            //        echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }

    /**
     * deletes a particular User's earliest tokens
     * @param int $user_id
     */
    public static function deleteEarliestUserAccessToken(int $user_id){
        $data = DB::table("oauth_access_tokens")
            ->where('user_id', $user_id)
            ->orderBy('created_at', 'ASC')
            ->get();

        if(count($data) > 0)return self::finalizeTokenDeletion($data);
    }

    /**
     * deletes all earliest token entries of a particular user
     * @param $tokens
     */
    public static function finalizeTokenDeletion($tokens){
        $total_counts = count($tokens);
        $accessN = ($total_counts - 1); //leave only but the last record
        $Tokens = $tokens; //get the actual data
        $token = [];

        if($accessN > 0){
            for($i = 0; $i < $accessN; $i++){
                $token = $Tokens[$i];
                $id = $token->id;
                $user_id = $token->user_id;

                DB::table('oauth_access_tokens')
                    ->where('id', $id)
                    ->where('user_id', $user_id)
                    ->delete();
            }
        }
    }

    /**
     * validate phone numbers
     * @param string $phone
     * @return \Illuminate\Http\JsonResponse
     */
    public static function validatePhone(string $phone){
        if(strlen($phone) > 11 || strlen($phone) < 11)return Helper::jsonResponse(false, 'Phone number should contain 11 valid numbers.', 400);
        if(!is_numeric($phone))return Helper::jsonResponse(false, 'Invalid phone number.', 400);
    }


}
