<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'input_id', 'merchant_id', 'customer_id', 'status_id', 'amount'
    ];
}
