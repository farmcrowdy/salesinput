<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Input extends Model
{
    protected $fillable = [
        'name', 'quantity', 'input_type_id', 'payment_method_id'
    ];
}
