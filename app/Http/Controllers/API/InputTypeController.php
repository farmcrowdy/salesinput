<?php

namespace App\Http\Controllers\API;

use App\Helper;
use App\Models\InputType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InputTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input_types = InputType::all();

        if(sizeof($input_types) > 0) {
            return Helper::jsonResponse(true, 'Retrieved Input Type records successfully.', 200, $input_types);
        } else {
            return Helper::jsonResponse(false, 'Failed to retrieve Input Type records', 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_type = InputType::create([
            'name' => $request->name,
            'description' => $request->description,
            'status_id' => $request->status_id,
        ]);

        if(!is_null($input_type)) {
            return Helper::jsonResponse(true, 'Created Input Type record successfully.', 200, $input_type);
        } else {
            return Helper::jsonResponse(false, 'Failed to create Input Type record', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InputType  $inputType
     * @return \Illuminate\Http\Response
     */
    public function show(InputType $inputType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InputType  $inputType
     * @return \Illuminate\Http\Response
     */
    public function edit(InputType $inputType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InputType  $inputType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InputType $inputType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InputType  $inputType
     * @return \Illuminate\Http\Response
     */
    public function destroy(InputType $inputType)
    {
        //
    }
}
