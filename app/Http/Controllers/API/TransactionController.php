<?php

namespace App\Http\Controllers\API;

use App\Helper;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::all();
        if(sizeof($transactions) > 0) {
            return Helper::jsonResponse(true, 'Transaction Records retrieved successfully.', 200, $transactions);
        } else {
            return Helper::jsonResponse(false, 'Failed to retrieve Transaction Records', 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction = Transaction::create([
           'input_id' => $request->input_id,
           'merchant_id' => $request->merchant_id,
           'customer_id' => $request->customer_id,
           'status_id' => $request->status_id,
           'amount' => $request->amount
        ]);

        if(!is_null($transaction)) {
            return Helper::jsonResponse(true, 'Transaction Record created successfully.', 200, $transaction);
        } else {
            return Helper::jsonResponse(false, 'Failed to create Transaction Record', 400);
        }
    }

    public function merchant_transactions($merchant_id) {
        $transactions = Transaction::where('merchant_id', $merchant_id)->get();
        if(sizeof($transactions) > 0) {
            return Helper::jsonResponse(true, 'Merchant Transaction Records retrieved successfully.', 200, $transactions);
        } else {
            return Helper::jsonResponse(false, 'Merchant has no Transaction Records', 400);
        }
    }


    public function customer_transactions($customer_id) {
        $transactions = Transaction::where('customer_id', $customer_id)->get();
        if(sizeof($transactions) > 0) {
            return Helper::jsonResponse(true, 'Customer Transaction Records retrieved successfully.', 200, $transactions);
        } else {
            return Helper::jsonResponse(false, 'Customer has no Transaction Records', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
