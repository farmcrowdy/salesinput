<?php

namespace App\Http\Controllers\API;

use App\Helper;
use App\Models\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rules\In;

class InputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inputs = Input::all();

        if(sizeof($inputs) > 0) {
            return Helper::jsonResponse(true, 'Retrieved Input records successfully.', 200, $inputs);
        } else {
            return Helper::jsonResponse(false, 'Failed to retrieve Input records', 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::create([
            'name' => $request->name,
            'quantity' => $request->quantity,
            'input_type_id' => $request->input_type_id,
            'payment_method' => $request->payment_method
        ]);

        if(!is_null($input)) {
            return Helper::jsonResponse(true, 'Created Input record successfully.', 200, $input);
        } else {
            return Helper::jsonResponse(false, 'Failed to create Input record successfully.', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Input  $input
     * @return \Illuminate\Http\Response
     */
    public function show(Input $input)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Input  $input
     * @return \Illuminate\Http\Response
     */
    public function edit(Input $input)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Input  $input
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Input $input)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Input  $input
     * @return \Illuminate\Http\Response
     */
    public function destroy(Input $input)
    {
        //
    }
}
