<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', );

//Route::middleware('auth:api')->group('user', function () {
//    Route::get('/', 'API/UsersController@all');
//});

Route::post('/register', 'API\AuthController@register');
Route::post('/login', 'API\AuthController@login');
Route::post('/logout', 'API\AuthController@logout');

Route::prefix('user')->group(function () {
    Route::get('/', 'API\UsersController@all');
    Route::get('all', 'API\UsersController@all');
    Route::post('store', 'API\UsersController@store');
});

Route::prefix('roles')->group(function () {
    Route::get('/', 'API\RoleController@index');
    Route::post('create', 'API\RoleController@store');
});

Route::prefix('status')->group(function () {
    Route::get('/', 'API\StatusController@index');
    Route::post('create', 'API\StatusController@store');
});

Route::prefix('inputs')->group(function () {
    Route::get('/', 'API\InputController@index');
    Route::post('create', 'API\InputController@store');
});

Route::prefix('input_types')->group(function () {
    Route::get('/', 'API\InputTypeController@index');
    Route::post('create', 'API\InputTypeController@store');
});

Route::prefix('payment_methods')->group(function () {
    Route::get('/', 'API\PaymentMethodController@index');
    Route::post('create', 'API\PaymentMethodController@store');
});

Route::prefix('transactions')->group(function () {
    Route::get('/', 'API\TransactionController@index');
    Route::post('create', 'API\TransactionController@store');
    Route::get('merchant/{merchant_id}', 'API\TransactionController@merchant_transactions');
    Route::get('customer/{customer_id}', 'API\TransactionController@customer_transactions');
});
